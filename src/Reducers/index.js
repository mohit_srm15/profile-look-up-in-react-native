import {ACTIONS} from '../Actions/Action';

const initialState = {
  userData: {},
  loading: false,
};

export function rootReducer(state = initialState, action) {
  if (action.type === ACTIONS.ADD_USER_DATA) {
    state = {...state, userData: action.payload};
  }
  if (action.type === ACTIONS.LOADING) {
    state = {...state, loading: action.payload};
  }
  return state;
}
