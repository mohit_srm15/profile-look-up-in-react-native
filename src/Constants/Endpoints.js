export const ENDPOINTS = {
  PHOTOS: {
    METHOD: 'get',
    URL: '/users/',
  },
  USERS: {
    METHOD: 'get',
    URL: '/search/users/',
  },
};
