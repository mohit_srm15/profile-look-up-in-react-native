import React from 'react';
import Axios from 'axios';

import {CONSTANTS} from '../Constants/Constant';
import {ENDPOINTS} from '../Constants/Endpoints';

export async function getUserList(searchKeyword, page) {
  let userList = await Axios[ENDPOINTS.USERS.METHOD](
    `${CONSTANTS.UNSPLASH_CONFIG.BASE_URL}${ENDPOINTS.USERS.URL}`,
    {
      params: {
        client_id: CONSTANTS.UNSPLASH_CONFIG.UNSPLASH_ACCESS_KEY,
        query: searchKeyword,
        page: page ? page : 1,
        perPage: 10,
      },
    },
  );
  return userList;
}

export async function getUserPhotos(userName, pageNumber) {
  let getPhotos = await Axios[ENDPOINTS.USERS.METHOD](
    `${CONSTANTS.UNSPLASH_CONFIG.BASE_URL}${ENDPOINTS.PHOTOS.URL}${userName}/photos/`,
    {
      params: {
        client_id: CONSTANTS.UNSPLASH_CONFIG.UNSPLASH_ACCESS_KEY,
        perPage: 10,
        page: pageNumber,
      },
    },
  );
  return getPhotos;
}
