import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import Splash from './Screens/Splash';
import Search from './Screens/Search';
import UserProfile from './Screens/Profile';

const appNavigations = createStackNavigator(
  {
    Splash: {screen: Splash},
    Search: {screen: Search},
    UserProfile: {screen: UserProfile},
  },
  {
    headerMode: 'none',
  },
);

export const AppContainer = createAppContainer(appNavigations);
