import {ACTIONS} from './Action';

export function setLoading(payload) {
  return {
    type: ACTIONS.LOADING,
    payload: payload,
  };
}

export function addUserData(payload) {
  return {
    type: ACTIONS.ADD_USER_DATA,
    payload: payload,
  };
}
