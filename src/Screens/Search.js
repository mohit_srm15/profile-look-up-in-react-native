import React, {useState} from 'react';
import {
  View,
  TextInput,
  ScrollView,
  Platform,
  KeyboardAvoidingView,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

import {BoldText} from '../Components/Common/StyledText';
import Color from '../Styles/Color';
import SearchList from '../Components/SearchList';

function Search(props) {
  const [searchKeyword, setSearchKeyword] = useState('');
  return (
    <View style={{flex: 1}}>
      <View
        style={{
          flex: Platform.OS == 'ios' ? 0.1 : 0.07,
          justifyContent: 'flex-end',
          alignItems: 'center',
          paddingBottom: 10,
          backgroundColor: Color.gray06,
        }}>
        <BoldText
          style={{
            color: Color.headerColor,
            fontWeight: '600',
            fontSize: 25,
            letterSpacing: 2,
          }}>
          Look Up !
        </BoldText>
      </View>
      <View style={{flex: 0.07}}>
        <View
          style={{
            justifyContent: 'space-between',
            alignItems: 'flex-start',
            paddingHorizontal: 20,
            flexDirection: 'row',
            backgroundColor: Color.gray04,
            borderBottomLeftRadius: 30,
            borderBottomRightRadius: 30,
            height: '100%',
          }}>
          <KeyboardAvoidingView style={{width: '90%'}}>
            <View style={{height: '100%', justifyContent: 'center'}}>
              <TextInput
                placeholder={'Search'}
                placeholderTextColor={'white'}
                onChangeText={(value) => setSearchKeyword(value)}
                value={searchKeyword}
                style={{
                  color: 'white',
                  fontSize: 20,
                  justifyContent: 'center',
                  alignSelf: 'center',
                  width: '90%',
                }}
                editable={true}
              />
            </View>
          </KeyboardAvoidingView>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              alignSelf: 'center',
            }}>
            <Icon name={'search1'} size={20} color={'white'} />
          </View>
        </View>
      </View>
      <ScrollView style={{flex: 1}}>
        <SearchList
          searchKeyword={searchKeyword}
          navigation={props.navigation}
        />
      </ScrollView>
    </View>
  );
}

export default Search;
