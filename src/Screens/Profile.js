import React, {useState, useEffect} from 'react';
import {View, ScrollView, TouchableOpacity, Platform} from 'react-native';
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/AntDesign';

import Color from '../Styles/Color';
import {BoldText} from '../Components/Common/StyledText';
import ProfileView from '../Components/Profile';

function UserProfile(props) {
  const [userData, setUserData] = useState({});

  useEffect(() => {
    if (props && props.state && props.state.userData) {
      setUserData(props.state.userData);
    }
  }, [props]);

  return (
    <View style={{flex: 1}}>
      <View
        style={{
          flex: Platform.OS == 'ios' ? 0.1 : 0.07,
          justifyContent: 'flex-end',
          alignItems: 'center',
          paddingBottom: 10,
          backgroundColor: Color.gray06,
        }}>
        <View style={{flexDirection: 'row', flex: 1, alignItems: 'flex-end'}}>
          <View
            style={{
              flexDirection: 'column',
              flex: 0.2,
              paddingLeft: 10,
            }}>
            <TouchableOpacity onPress={() => props.navigation.goBack(null)}>
              <Icon name={'left'} size={25} color={Color.gray04} />
            </TouchableOpacity>
          </View>
          <View style={{flexDirection: 'column', flex: 0.8}}>
            <BoldText
              style={{
                color: Color.headerColor,
                fontWeight: '600',
                fontSize: 25,
                letterSpacing: 2,
              }}>
              {userData ? userData.username : 'Profile'}
            </BoldText>
          </View>
        </View>
      </View>
      <ScrollView style={{flex: 1}}>
        <ProfileView
          userData={userData ? userData : {}}
          navigation={props.navigation}
        />
      </ScrollView>
    </View>
  );
}

const mapStateToProps = (state) => {
  return {state};
};

UserProfile = connect(mapStateToProps)(UserProfile);
export default UserProfile;
