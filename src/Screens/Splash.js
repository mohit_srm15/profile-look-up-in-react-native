import React, {useEffect} from 'react';
import {View} from 'react-native';
import {StackActions, NavigationActions} from 'react-navigation';

import {BoldText} from '../Components/Common/StyledText';

function Splash(props) {
  useEffect(() => {
    setTimeout(() => {
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({routeName: 'Search'})],
      });
      props.navigation.dispatch(resetAction);
    }, 3000);
  }, [0]);

  return (
    <View style={{flex: 1}}>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'white',
        }}>
        <View
          style={{
            width: '100%',
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <BoldText>Welcome To Unsplash App</BoldText>
        </View>
      </View>
    </View>
  );
}

export default Splash;
