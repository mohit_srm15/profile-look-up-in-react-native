import React, {Component} from 'react';
import {Text} from 'react-native';

export class BoldText extends Component {
  render() {
    let {style} = this.props;
    if (style == null || style === undefined) {
      style = {};
    }
    return (
      <Text
        style={[
          {color: '#212224', fontSize: 20, fontWeight: '500'},
          style,
        ]}>
        {this.props.children}
      </Text>
    );
  }
}

export class LightText extends Component {
  render() {
    let {style} = this.props;
    if (style == null || style === undefined) {
      style = {};
    }
    return (
      <Text
        style={[{color: '#585858', fontWeight: '400'}, style]}
        numberOfLines={
          this.props.numberOfLines ? this.props.numberOfLines : null
        }>
        {this.props.children}
      </Text>
    );
  }
}
