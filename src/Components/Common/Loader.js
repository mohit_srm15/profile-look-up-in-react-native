import React from 'react';
import {StyleSheet, View, ImageBackground} from 'react-native';
import {connect} from 'react-redux';

import {LightText} from './StyledTexts';
import Color from '../../Styles/Color';

function Loader(props) {
  return props.state.loading ? (
    <View style={styles.loader}>
      <ImageBackground
        source={require('../../../assets/splashy-loader.gif')}
        style={{width: '100%', height: '100%', backgroundColor: 'white'}}
        resizeMode={'contain'}>
        <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
          <LightText style={{fontSize: 25, color: Color.gray04}}>
            Loading ...
          </LightText>
        </View>
      </ImageBackground>
    </View>
  ) : null;
}

const mapStateToProps = (state) => {
  return {state};
};
export default Loader = connect(mapStateToProps)(Loader);

const styles = StyleSheet.create({
  loader: {
    height: '100%',
    width: '100%',
    position: 'absolute',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
