import React from 'react';
import {Image, View} from 'react-native';

import Color from '../../Styles/Color';
import {BoldText, LightText} from '../Common/StyledText';
import PhotoGrid from './PhotoGrid';

function ProfileView(props) {
  let {userData} = props;

  return (
    <View style={{flex: 1}}>
      <View style={{paddingHorizontal: 10, paddingVertical: 30}}>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <View style={{flex: 0.35, flexDirection: 'column'}}>
            <View
              style={{
                flexDirection: 'column',
                flex: 0.3,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={
                  userData &&
                  userData.profile_image &&
                  userData.profile_image.medium
                    ? {uri: userData.profile_image.medium}
                    : 'N/A'
                }
                style={{
                  height: 100,
                  width: 100,
                  borderRadius: 50,
                  borderColor: Color.gray02,
                }}
                resizeMode={'cover'}
              />
            </View>
          </View>
          <View
            style={{
              flexDirection: 'column',
              flex: 0.65,
              justifyContent: 'center',
            }}>
            <View
              style={{
                flexDirection: 'row',
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  flexDirection: 'column',
                  flex: 1.2,
                  alignItems: 'center',
                }}>
                <BoldText style={{fontSize: 25}}>
                  {userData.total_collections ? userData.total_collections : 0}
                </BoldText>
                <LightText
                  style={{
                    fontSize: 18,
                    color: Color.gray04,
                    fontWeight: '500',
                    marginVertical: 5,
                  }}>
                  Collections
                </LightText>
              </View>
              <View
                style={{
                  flexDirection: 'column',
                  flex: 0.9,
                  alignItems: 'center',
                }}>
                <BoldText style={{fontSize: 25}}>
                  {userData.total_photos ? userData.total_photos : 0}
                </BoldText>
                <LightText
                  style={{
                    fontSize: 18,
                    color: Color.gray04,
                    fontWeight: '500',
                    marginVertical: 5,
                  }}>
                  Posts
                </LightText>
              </View>
              <View
                style={{
                  flexDirection: 'column',
                  flex: 0.9,
                  alignItems: 'center',
                }}>
                <BoldText style={{fontSize: 25}}>
                  {userData.total_likes ? userData.total_likes : 0}
                </BoldText>
                <LightText
                  style={{
                    fontSize: 18,
                    color: Color.gray04,
                    fontWeight: '500',
                    marginVertical: 5,
                  }}>
                  Likes
                </LightText>
              </View>
            </View>
          </View>
        </View>
        <View style={{marginTop: 30, paddingHorizontal: 10}}>
          <View>
            <BoldText style={{fontSize: 25}}>
              {userData.name ? userData.name : 'N/A'}
            </BoldText>
          </View>
          <View>
            <LightText style={{fontSize: 20, marginVertical: 5}}>
              {userData.bio ? userData.bio : null}
            </LightText>
          </View>
        </View>
        <View style={{paddingHorizontal: 10}}>
          <View style={{flexDirection: 'row', flex: 1, alignItems: 'center'}}>
            <BoldText
              style={{
                fontSize: 25,
                fontWeight: '600',
                letterSpacing: 2,
                color: Color.gray04,
              }}>
              Gallery
            </BoldText>
            {/*<LightText style={{marginHorizontal: 10, fontSize: 20}}>*/}
            {/*  {`( ${userData.total_photos ? userData.total_photos : 0} )`}*/}
            {/*</LightText>*/}
          </View>
        </View>
        <View>
          <PhotoGrid
            userName={userData.username ? userData.username : ''}
            navigation={props.navigation}
          />
        </View>
      </View>
    </View>
  );
}

export default ProfileView;
