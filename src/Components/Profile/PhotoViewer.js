import React, {useState, useEffect} from 'react';
import {View, Modal, TouchableWithoutFeedback, Image} from 'react-native';
import GestureRecognizer from 'react-native-swipe-gestures';

import Color from '../../Styles/Color';

function PhotoViewer(props) {
  let {photoList, setModalVisible, modalVisible, activeIndex} = props;

  const [activePhotoIndex, setActivePhotoIndex] = useState(activeIndex);

  function handleSwipeLeft() {
    if (activePhotoIndex < photoList.length - 1 && activePhotoIndex >= 0) {
      setActivePhotoIndex(activePhotoIndex + 1);
    }
  }

  function handleSwipeRight() {
    if (activePhotoIndex <= photoList.length - 1 && activePhotoIndex > 0) {
      setActivePhotoIndex(activePhotoIndex - 1);
    }
  }

  return (
    <View>
      <Modal animationType="slide" transparent={true} visible={modalVisible}>
        <TouchableWithoutFeedback onPress={() => setModalVisible(false)}>
          <GestureRecognizer
            style={{
              flex: 1,
              backgroundColor: 'rgba(0,0,0, 0.8)',
            }}
            config={{
              velocityThreshold: 0.3,
              directionalOffsetThreshold: 80,
            }}
            onSwipeLeft={() => handleSwipeLeft()}
            onSwipeRight={() => handleSwipeRight()}
            onSwipeDown={() => setModalVisible(false)}>
            <TouchableWithoutFeedback>
              <View
                onPress={() => setModalVisible(false)}
                style={{
                  shadowColor: '#451B2D',
                  shadowOpacity: 0.5,
                  height: '100%',
                  width: '100%',
                  backgroundColor: 'transparent',
                }}>
                <View style={{flex: 0.03}} />
                <View>
                  {photoList && photoList.length > 0 ? (
                    <Image
                      source={
                        photoList[activePhotoIndex] &&
                        photoList[activePhotoIndex].urls &&
                        photoList[activePhotoIndex].urls.regular
                          ? {uri: photoList[activePhotoIndex].urls.regular}
                          : 'N/A'
                      }
                      style={{
                        height: '100%',
                        width: '100%',
                        borderColor: Color.gray02,
                      }}
                      resizeMode={'cover'}
                    />
                  ) : null}
                </View>
              </View>
            </TouchableWithoutFeedback>
          </GestureRecognizer>
        </TouchableWithoutFeedback>
      </Modal>
    </View>
  );
}

export default PhotoViewer;
