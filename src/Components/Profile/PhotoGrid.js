import React, {useEffect, useState} from 'react';
import {Image, View, TouchableOpacity, FlatList} from 'react-native';

import {getUserPhotos} from '../../API/User';
import Color from '../../Styles/Color';
import PhotoViewer from './PhotoViewer';

function PhotoGrid(props) {
  const [photoList, setPhotoList] = useState([]);
  const [pageNumber, setPageNumber] = useState(1);
  const [retreiveMoreStatus, setRetreiveMoreStatus] = useState(true);
  const [modalVisible, setModalVisible] = useState(false);
  const [activeIndex, setActiveIndex] = useState(0);

  useEffect(() => {
    if (props && props.userName) {
      getUserPhotoList();
    }
  }, [props]);

  async function getUserPhotoList() {
    try {
      let photoData = await getUserPhotos(props.userName, 1);
      setPhotoList(photoData.data);
    } catch (error) {
      console.log(' error ', error);
    }
  }

  async function fetchMore() {
    if (retreiveMoreStatus) {
      try {
        let photoData = await getUserPhotos(props.userName, pageNumber + 1);
        setPageNumber(pageNumber + 1);
        if (photoData.data && photoData.data.length < 10) {
          setRetreiveMoreStatus(false);
        }
        setPhotoList([...photoList, ...photoData.data]);
      } catch (error) {
        console.log(' error ', error);
      }
    }
  }

  return (
    <View style={{marginVertical: 10}}>
      <FlatList
        data={photoList}
        keyExtractor={(item, index) => {
          `photo-grid-${index}`;
        }}
        onEndReachedThreshold={0.5}
        onEndReached={() => fetchMore()}
        renderItem={({item, index}) => {
          console.log( "i ", index );
          return (
            <TouchableOpacity
              onPress={() => {
                setModalVisible(true);
                setActiveIndex(index);
              }}>
              <View style={{margin: 2}}>
                <Image
                  source={
                    item && item.urls && item.urls.regular
                      ? {uri: item.urls.regular}
                      : 'N/A'
                  }
                  style={{
                    height: 400,
                    width: '100%',
                    borderColor: Color.gray02,
                  }}
                  resizeMode={'cover'}
                />
              </View>
            </TouchableOpacity>
          );
        }}
      />
      <PhotoViewer
        modalVisible={modalVisible}
        setModalVisible={(status) => setModalVisible(status)}
        photoList={photoList}
        activeIndex={activeIndex}
      />
    </View>
  );
}

export default PhotoGrid;
