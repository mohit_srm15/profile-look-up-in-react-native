import React, {useState, useEffect} from 'react';
import {View, Image, TouchableOpacity} from 'react-native';

import {getUserList} from '../API/User';
import {BoldText, LightText} from './Common/StyledText';
import Color from '../Styles/Color';
import store from '../Store';
import {addUserData} from '../Actions/ActionCreator';

function SearchList(props) {
  const [userList, setUserList] = useState([]);
  const [pageNumber, setPageNumber] = useState(1);
  const [loadMoreStatus, setLoadMoreStatus] = useState(true);

  useEffect(() => {
    if (props && props.searchKeyword) {
      getUserData();
    }
  }, [props]);

  async function getUserData() {
    try {
      let userData = await getUserList(props.searchKeyword, pageNumber);
      if (userData && userData.data && userData.data.results) {
        setUserList(userData.data.results);
      }
    } catch (error) {
      console.log(' error ', error);
    }
  }

  function handleUserClick(user) {
    store.dispatch(addUserData(user));
    props.navigation.navigate('UserProfile');
  }

  return (
    <View style={{paddingHorizontal: 20, marginTop: 20}}>
      {userList.length > 0 ? (
        userList.map((user,i) => {
          return (
            <TouchableOpacity onPress={() => handleUserClick(user)} key={`user-${i}`}>
              <View
                style={{
                  flexDirection: 'row',
                  flex: 1,
                  borderWidth: 1,
                  paddingVertical: 20,
                  borderColor: Color.gray02,
                  borderRadius: 20,
                  marginVertical: 15,
                }}>
                <View
                  style={{
                    flexDirection: 'column',
                    flex: 0.3,
                    // borderWidth: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  {
                    user.profile_image?
                        <Image
                            source={
                              user.profile_image && user.profile_image.medium
                                  ? {uri: user.profile_image.medium}
                                  : 'N/A'
                            }
                            style={{
                              height: 80,
                              width: 80,
                              borderRadius: 40,
                              borderWidth: 1,
                              borderColor: Color.gray02,
                            }}
                            resizeMode={'cover'}
                        />
                        :
                        <BoldText>No Photos available</BoldText>
                  }
                </View>
                <View
                  style={{
                    flexDirection: 'column',
                    flex: 0.7,
                    justifyContent: 'center',
                  }}>
                  <BoldText style={{color: Color.gray04, fontSize: 23}}>
                    {user.username ? user.username : 'N/A'}
                  </BoldText>
                  <LightText
                    style={{
                      color: Color.gray02,
                      fontSize: 20,
                      marginVertical: 5,
                    }}>
                    {user.name ? user.name : 'N/A'}
                  </LightText>
                </View>
              </View>
            </TouchableOpacity>
          );
        })
      ) : (
        <BoldText>No results found</BoldText>
      )}
      {/*<View style={{marginBottom: 10 , justifyContent: 'center' , alignItems: 'center'}}>*/}
      {/*  <TouchableOpacity style={{}} >*/}
      {/*    <BoldText>Load More</BoldText>*/}
      {/*  </TouchableOpacity>*/}
      {/*</View>*/}
    </View>
  );
}

export default SearchList;
