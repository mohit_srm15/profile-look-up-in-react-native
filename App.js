/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {View} from 'react-native';
import 'react-native-gesture-handler';
import {Provider} from 'react-redux';

import {AppContainer} from './src/Router';
import store from './src/Store';

function App() {
  return (
    <View style={{flex: 1}}>
      <Provider store={store}>
        <AppContainer />
      </Provider>
    </View>
  );
}

export default App;
