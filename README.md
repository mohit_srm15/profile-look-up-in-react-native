run $yarn to install all packages

To run app on:
IOS : $ cd ios && pod install && cd .. &&  yarn run ios
Android: $ yarn run Android

to create android build release
$ yarn run release-build 

Regarding gesture to swipe images:

1. Click on image to get fullscreen preview.
2. Swipe left to move to next image.
3  Swipe right for previous image.
4. Swipe down to close image preview.

